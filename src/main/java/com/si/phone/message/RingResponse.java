package com.si.phone.message;

/**
 * Created by aacs on 2018. 01. 06..
 */
public class RingResponse extends BaseMessage {
    @Override
    public String getType() {
        return "RingResponse";
    }

    public String getRingId() {
        return ringId;
    }

    public void setRingId(String ringId) {
        this.ringId = ringId;
    }

    private String ringId;
}
