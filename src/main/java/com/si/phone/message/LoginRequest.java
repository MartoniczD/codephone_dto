package com.si.phone.message;

/**
 * Created by aacs on 2018. 01. 06..
 */
public class LoginRequest extends BaseMessage {
    public LoginRequest() {

    }

    public LoginRequest(String email, String password) {
        this.password = password;
        this.email = email;
    }

    @Override
    public String getType() {
        return "LoginRequest";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String email;
    private String password;
}
