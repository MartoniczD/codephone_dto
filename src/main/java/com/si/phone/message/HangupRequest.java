package com.si.phone.message;

/**
 * Created by aacs on 2018. 01. 07..
 */
public class HangupRequest extends BaseMessage {
    private String ringId;
    @Override
    public String getType() {
        return "HangupRequest";
    }

    public String getRingId() {
        return ringId;
    }

    public void setRingId(String ringId) {
        this.ringId = ringId;
    }

    public HangupRequest() {
        //NOP
    }

    public HangupRequest(String ringId) {
        this.ringId = ringId;
    }
}
