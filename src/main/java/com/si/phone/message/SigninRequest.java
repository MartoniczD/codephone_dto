package com.si.phone.message;

/**
 * Created by aacs on 2018. 01. 07..
 */
public class SigninRequest extends BaseMessage {
    @Override
    public String getType() {
        return "SigninRequest";
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    private String uid;
}
