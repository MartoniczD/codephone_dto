package com.si.phone.message;

/**
 * Created by aacs on 2018. 01. 07..
 */
public class ErrorResponse {
    private String apartmanId;

    public ErrorResponse() {
        //NOP
    }

    public ErrorResponse(String apartmanId) {

        this.apartmanId = apartmanId;
    }

    public String getApartmanId() {
        return apartmanId;
    }

    public void setApartmanId(String apartmanId) {
        this.apartmanId = apartmanId;
    }
}
