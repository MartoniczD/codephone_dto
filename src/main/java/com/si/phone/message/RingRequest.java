package com.si.phone.message;

/**
 * Created by aacs on 2018. 01. 06..
 */
public class RingRequest extends BaseMessage {
    @Override
    public String getType() {
        return "RingRequest";
    }

    public byte getApartmanId() {
        return apartmanId;
    }

    public void setApartmanId(byte apartmanId) {
        this.apartmanId = apartmanId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    private byte apartmanId;
    private String uid;
}
