package com.si.phone.message;

public class OpenRequest extends BaseMessage {
    public String getType() {
        return "OpenRequest";
    }

    private String ringId;

    public String getRingId() {
        return ringId;
    }

    public void setRingId(String ringId) {
        this.ringId = ringId;
    }

    public OpenRequest() {
        //NOP
    }

    public OpenRequest(String ringId) {
        this.ringId = ringId;
    }
}
