package com.si.phone.message;

/**
 * Created by aacs on 2018. 01. 07..
 */
public class SigninResponse extends BaseMessage {
    @Override
    public String getType() {
        return "SigninResponse";
    }

    public Byte[] getApartmants() {
        return apartmants;
    }

    public void setApartmants(Byte[] apartmants) {
        this.apartmants = apartmants;
    }

    private Byte[] apartmants;
}
