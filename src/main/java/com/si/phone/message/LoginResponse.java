package com.si.phone.message;

/**
 * Created by aacs on 2018. 01. 06..
 */
public class LoginResponse extends BaseMessage {
    public LoginResponse() {

    }

    public LoginResponse(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String getType() {
        return "LoginResponse";
    }

    private boolean success;
}
