package com.si.phone.message;

/**
 * Created by aacs on 2018. 01. 07..
 */
public class AbortRequest extends BaseMessage {
    @Override
    public String getType() {
        return "AbortRequest";
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public byte getApartmanId() {
        return apartmanId;
    }

    public void setApartmanId(byte apartmanId) {
        this.apartmanId = apartmanId;
    }

    private String uid;
    private byte apartmanId;
}
