package com.si.phone.message;

/**
 * Created by aacs on 2018. 01. 07..
 */
public class PickupRequest extends BaseMessage {
    @Override
    public String getType() {
        return "PickupRequest";
    }

    public PickupRequest(){

    }

    public PickupRequest(String ringId){
        this.ringId = ringId;

    }

    private String ringId;

    public String getRingId() {
        return ringId;
    }

    public void setRingId(String ringId) {
        this.ringId = ringId;
    }
}
