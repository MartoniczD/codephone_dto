package com.si.phone.message;

/**
 * Created by aacs on 2018. 01. 07..
 */
public class AbortResponse extends BaseMessage{
    @Override
    public String getType() {
        return "AbortResponse";
    }

}
