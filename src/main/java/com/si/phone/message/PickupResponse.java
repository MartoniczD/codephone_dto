package com.si.phone.message;

/**
 * Created by aacs on 2018. 01. 07..
 */
public class PickupResponse extends BaseMessage {
    @Override
    public String getType() {
        return "PickupResponse";
    }

    public String getRingId() {
        return ringId;
    }

    public void setRingId(String ringId) {
        this.ringId = ringId;
    }

    private String ringId;

    public PickupResponse() {
        //NOP
    }

    public PickupResponse(String ringId) {
        this.ringId = ringId;
    }
}
