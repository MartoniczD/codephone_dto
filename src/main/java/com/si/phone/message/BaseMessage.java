package com.si.phone.message;

/**
 * Created by aacs on 2018. 01. 06..
 */
public abstract class BaseMessage {
    public abstract String getType();

    public void setType(String type) {
        this.type = type;
    }

    private String type;
}
